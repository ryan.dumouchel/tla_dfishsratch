------------------------------ MODULE scratch ------------------------------
EXTENDS Integers, TLC, Sequences, FiniteSets

Events == {"A", "B", "C", "D", "E",  "F", "G", "H", "I"}

SmachineParents == [s |-> "", s1 |-> "s", s2 |-> "s", s11 |-> "s1", s12 |-> "s1", s21 |-> "s2", s211 |-> "s21"]
SmachineInit == [s |-> "s1", s1 |-> "s11", s2 |-> "s21", s11 |-> "", s12 |-> "", s21 |-> "s211", s211 |-> ""]

\*
MapThenFoldSet(op(_,_), base, f(_), choose(_), S) ==
  LET iter[s \in SUBSET S] ==
        IF s = {} THEN base
        ELSE LET x == choose(s)
             IN  op(f(x), iter[s \ {x}])
  IN  iter[S]

FoldSet(op(_,_), base, set) ==

   MapThenFoldSet(op, base, LAMBDA x : x, LAMBDA s : CHOOSE x \in s : TRUE, set)

Or(a,b) == 
  a /\ b


\* Return a sequence of states to the root, ordered
RootPath(state,parents) == LET
   path == <<>>
   
   RECURSIVE Helper(_, _)
   Helper(state_,path_) == IF parents[state_] = "" THEN Append(path_,state_) ELSE
   Helper(parents[state_], Append(path_,state_))
 IN Helper(state,path)
  
\* Return the set of states to the root, not ordered
Root(state,parents) == LET
   path == {}
   
   RECURSIVE Helper(_, _)
   Helper(state_,path_) == IF parents[state_] = "" THEN path_ \union {state_} ELSE
   Helper(parents[state_], path_ \union {state_})
 IN Helper(state,path) 

\* Lca - Lowest common ancestor of two states, used for transition path
Lca(state1,state2,parents) == LET
   path1 == Root(state1,parents)
   path2 == RootPath(state2,parents)
   RECURSIVE Helper(_, _) 
   Helper(path1_,path2_) == IF Head(path2_) \in path1_ THEN Head(path2_) ELSE
   Helper(path1_, Tail(path2_))
   
 IN 
   Helper(path1,path2)

Reverse(seq) ==
   [ i \in 1..Len(seq) |-> seq[(Len(seq) - i) + 1] ]



FindNext(state,event,parents,Op(_,_)) == LET
    rootpath == RootPath(state,parents)
    RECURSIVE Helper(_)
    Helper(rootpath_) == IF Op(Head(rootpath_),event) = "super" THEN
    Helper(Tail(rootpath_)) ELSE Op(Head(rootpath_),event)
  IN 
    Helper(rootpath)  
    
Transition1(state,event,parents,Op(_,_)) == LET
     nextstate == FindNext(state,event,parents,Op)
   IN
      IF nextstate \in DOMAIN parents THEN nextstate ELSE "none"  

ExitPath(start,event,parents,NextState(_,_)) == LET
   end == Transition1(start,event,parents,NextState)
   lca == Lca(start,end,parents)
   rootpath == RootPath(start,parents)
   exitpath == <<>>
   RECURSIVE Helper(_,_,_)
   Helper(lca_,rootpath_,exitpath_) == IF Head(rootpath_) = lca THEN exitpath_ ELSE
   Helper(lca_,Tail(rootpath_), Append(exitpath_,Head(rootpath_))) 
 IN
   IF end = "none" THEN <<start>> ELSE Helper(lca,rootpath,exitpath)


EnterPath1(start,event,parents,NextState(_,_),init) == LET
   enterpath == <<>>
   end == Transition1(start,event,parents,NextState)
   lca == Lca(start,end,parents)
   rootpath == RootPath(end,parents) 
   RECURSIVE Helper(_,_,_)
   Helper(lca_,rootpath_,enterpath_) == IF Head(rootpath_) = lca THEN enterpath_ ELSE
   Helper(lca_,Tail(rootpath_), Append(enterpath_,Head(rootpath_)))
  
 IN
   IF end = "none" THEN <<start>> ELSE Reverse(Helper(lca,rootpath,enterpath))
   \*Helper(lca,rootpath,enterpath)

ToSet(s) ==
  (*************************************************************************)
  (* The image of the given sequence s. Cardinality(ToSet(s)) <= Len(s)    *)
  (* see https://en.wikipedia.org/wiki/Image_(mathematics)                 *)
  (*************************************************************************)
  { s[i] : i \in DOMAIN s }


EnterPath(start,event,parents,NextState(_,_),init) == LET
   enterpath == <<>>
   end == Transition1(start,event,parents,NextState)
   lca == Lca(start,end,parents)
   rootpath == RootPath(end,parents) 
   RECURSIVE Helper(_,_,_)
   Helper(lca_,rootpath_,enterpath_) == IF Head(rootpath_) = lca THEN enterpath_ ELSE
   Helper(lca_,Tail(rootpath_), Append(enterpath_,Head(rootpath_)))
   RECURSIVE HelperInit(_,_,_)
   HelperInit(enterpath_,init_,end_) == IF init[end_] = "" THEN enterpath_ ELSE HelperInit(Append(enterpath_, init_[end_]),init_,init_[end_])  
 IN
   IF end = "none" THEN <<start>> ELSE HelperInit(Reverse(Helper(lca,rootpath,enterpath)),init,end)
   \*Helper(lca,rootpath,enterpath)

FinishState(start,event,parents,NextState(_,_),init) == LET
    
    \* Example S211 and B involves no enters
    reverseEnter == Reverse(EnterPath(start,event,parents,NextState,init)) 
    IN 
     IF reverseEnter = <<>> THEN start ELSE Head(reverseEnter)
     

RunPath(path, Op(_)) == 
      
      {Op(path[i]): i \in 1..Len(path)}
      
    
RunEnters(enterpath,Op(_)) == LET
      
      RECURSIVE Helper(_)
      Helper(enterpath_) == IF enterpath = <<>> THEN TRUE ELSE Helper(Tail(enterpath_))
    IN
      Helper(enterpath) 


  
  
SmachineTransition(state,event) ==
  
     CASE (state = "s") /\ (event = "E") -> "s11"
     [] (state = "s1") /\ (event = "C") -> "s2"
     [] (state = "s1") /\ (event = "F") -> "s211"
     [] (state = "s1") /\ (event = "A") -> "s1"
     [] (state = "s1") /\ (event = "B") -> "s11"
     [] (state = "s1") /\ (event = "D") -> "s"
     [] (state = "s11") /\ (event = "G") -> "s211"
     [] (state = "s11") /\ (event = "H") -> "s"
     [] (state = "s11") /\ (event = "D") -> "s1"
     [] (state = "s2") /\ (event = "F") -> "s11"
     [] (state = "s2") /\ (event = "C") -> "s1"
     [] (state = "s21") /\ (event = "A") -> "s21"
     [] (state = "s21") /\ (event = "C") -> "s1"
     [] (state = "s21") /\ (event = "B") -> "s211"
     [] (state = "s211") /\ (event = "D") -> "s21"
     [] (state = "s211") /\ (event = "H") -> "s"
     [] (state = "s") -> "done" \* need this to end recursion  in FindNext statement
     [] OTHER -> "super"        \* need this to enable recusion in FindNext  statement



SmachineExit(state,foo_) ==       
        CASE state = "s211"
           -> 2
        [] state = "s1"
           -> 0
        [] OTHER -> foo_

    
RunSmachineExits(path,state) == LET
      
      RECURSIVE Helper(_,_)
      Helper(path_,state_) == IF path_ = <<>> THEN state_ ELSE Helper(Tail(path_),SmachineExit(Head(path_),state_))
    IN
      Helper(path,state) 

SmachineEnter(state,foo_) ==       
        CASE state = "s211"
           -> 4
        [] OTHER -> foo_

RunSmachineEnters(path,state) == LET
     
      RECURSIVE Helper(_,_)
      Helper(path_,state_) == IF path_ = <<>> THEN state_ ELSE Helper(Tail(path_),SmachineEnter(Head(path_),state_))
    IN
      Helper(path,state) 



SmachineFinish(state,event,foo_) ==
     CASE (state = "s") /\ (event = "I")
         ->  0
     [] (state = "s1") /\ (event = "D")
         -> 1
     [] (state = "s11") /\ (event = "D") \*/\ (foo = 1)
         ->  1    
     [] (state = "s11") /\ (event = "D") \* /\ (foo = 1)
         -> 1 
     [] (state = "s2") /\ (event = "I") \*/\ (foo = 0)
         -> 1 
     [] OTHER -> foo_

\* Make state variable updates before leavng state, then make state variable update along exit and enter path
SmachineUpdate(state,event,statevar,exitpath,enterpath) == LET
         SmachineStateVarFinish ==  SmachineFinish(state,event,statevar) \* State variable before exiting
         SmachineStateVarExit == RunSmachineExits(exitpath,SmachineStateVarFinish)
         IN
            RunSmachineEnters(enterpath, SmachineStateVarExit) \* State variable after exit)
           

\* if I remove myself from am I still in set


LeafFilter(set,member) == LET
   find == {x \in set: member \subseteq x } 
  IN
   Cardinality(find)
     
LeafStates(parents) == LET
    allRootPaths == {Root(x,parents): x \in (DOMAIN parents)}
    uniqueRootPaths == {x \in allRootPaths: LeafFilter(allRootPaths,x) = 1}
    diff == allRootPaths \ uniqueRootPaths
    leaves ==   (UNION uniqueRootPaths) \ (UNION diff)
   IN
    leaves
  

RunPathz(path, Op(_)) == 
      
      {Op(path[i]): i \in 1..Len(path)}

FoldPath(result) ==
     FoldSet(Or,FALSE,result)

ExecPath(path, Op(_))  == LET
       pathresults == RunPath(path,Op)
       IN
         FoldPath(pathresults)


  
    
Eval22 == LeafStates(SmachineParents)    
Eval6 == {ExitPath(state,evt,SmachineParents,SmachineTransition): evt \in Events, state \in Eval22} 
Eval62 == {EnterPath(state,evt,SmachineParents,SmachineTransition,SmachineInit): evt \in Events, state \in Eval22} 
Eval64 == ExitPath("s211","C", SmachineParents,SmachineTransition)
Eval644 == EnterPath("s211","C", SmachineParents,SmachineTransition,SmachineInit)
Eval65 == RunSmachineExits(Eval64,0)
Eval655 == RunSmachineEnters(Eval644,Eval65)

Eval16 == {ExitPath(state,evt,SmachineParents,SmachineTransition): evt \in Events, state \in Eval22} 
Eval164 == ExitPath("s11","F", SmachineParents,SmachineTransition)
Eval1644 == EnterPath("s11","F", SmachineParents,SmachineTransition,SmachineInit)
Eval1645 == FinishState("s11","F", SmachineParents,SmachineTransition,SmachineInit)
Eval165 == RunSmachineExits(Eval164,0)
Eval1655 == RunSmachineEnters(Eval1644,Eval165)

Eval77 == SmachineUpdate("s11","F",0,Eval164,Eval1644) 


Eval63 == FoldSet(Or,FALSE,Eval65)
Eval66 == ToSet(EnterPath1("s211","C", SmachineParents,SmachineTransition,SmachineInit))
Eval7 == {Transition1(state,evt,SmachineParents,SmachineTransition): evt \in Events, state \in (DOMAIN SmachineParents)} 
Eval2 == LeafStates(SmachineParents)
Eval == FinishState("s11", "D", SmachineParents,SmachineTransition,SmachineInit)
Eval1 == Transition1("s11","G",SmachineParents,SmachineTransition)
test ==
{ {"s"},
     {"s", "s1"},
     {"s", "s2"},
     {"s", "s1", "s11"},
     {"s", "s1", "s12"},
     {"s", "s2", "s21"},
     {"s", "s2", "s21", "s211"} }
Eval4 == {x \in test: {"s","s1"} \subseteq x } 
Eval5 ==  LeafFilter(test,{"s","s1","s11"})    
=============================================================================
\* Modification History
\* Last modified Wed Nov 01 21:57:09 EDT 2023 by ryandumouchel
\* Created Mon Oct 09 13:04:52 EDT 2023 by ryandumouchel
